import csv
import json
import requests
from requests.exceptions import HTTPError


def top_items(data_local: [], item_type: str, list_length: int):
    """ Analyzes data for the Top items and returns them as an array. """

    # SRCIP also includes tcp port - Let's get rid of the port numbers first
    if item_type == 'SRCIP':
        for temp_row in data_local:
            temp_row[item_type] = str(temp_row[item_type]).split(':')[0]

    # Iterate through the data
    items = {}
    for row_local in data_local:
        x = 0
        # Count occurrences
        if row_local[item_type] in items:
            x = int(items[row_local[item_type]])
            items[row_local[item_type]] = x + 1
        else:
            items.update({row_local[item_type]: 1})

    # Sort items by occurrence
    sorted_items = sorted(items, key=items.get, reverse=True)
    results = []

    # Distill Top list to return
    for x in range(0, list_length):
        result = {sorted_items[x]: items[sorted_items[x]]}
        results.append(result)
    return results


def ip_location(top_ip_addresses_local: []):
    """ Runs SRCIP fields against geo-location database and save result in a CSV file. """
    with open('config.json') as json_data_file:
        config_data = json.load(json_data_file)
    # get ipdata.co API key
    conf_api_key = config_data['ipdata.co']['key']

    # Open CSV file
    with open('country-stats.csv', mode='w') as csv_file:
        fieldnames = ['AccessCount', 'IP', 'Country']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        # Get Country info for each IP address
        for ip_address in top_ip_addresses_local:
            ip = str(list(ip_address.keys())[0])
            request_url = 'https://api.ipdata.co/' + ip + '?api-key=' + conf_api_key
            try:
                response = requests.get(request_url, timeout=2)
            except HTTPError as http_err:
                print(f'HTTP error occurred: {http_err}')
            except Exception as err:
                print(f'Other error occurred: {err}')
            response_data = json.loads(response.content)

            # write row into CSV file
            writer.writerow({'AccessCount': ip_address[ip], 'IP': ip, 'Country': response_data['country_name']})


if __name__ == "__main__":
    """ Main Loop: Opens CSV file, creates user agent Top10, creates endpoint Top10, 
    and creates world map based on IP geo-location. """

    # In-memory object for the data
    data = []

    # Load data from CSV file
    print('Reading data from CSV file...')
    with open("elb-access-log.csv", 'r') as file:
        csv_file = csv.DictReader(file)
        for row in csv_file:
            data.append(dict(row))
    print('Done.')

    top_ip_addresses = top_items(data, 'SRCIP', 200)
    ip_location(top_ip_addresses)

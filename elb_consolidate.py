"""
This script combines AWS ELB Access Logs into a single CSV file.
--
AWS ELB Access Logs are stored as many *.log.gz files in a rather
deep folder structure with folders for the year, month, and day.
This script iterates through all *.log.gz files in the folder
structure recursively, parses all the log lines (without having
to unzip the files) and writes the data into a single csv file.
--
Parsed Fields:
TS          timestamp
PROT        protocol
SRCIP       source IP address and port
COMMAND     http request
AGENT       user agent string
"""

import glob
import gzip
import csv
from pathlib import Path


def parse_line(line: str):
    """ Parses given line and returns a dictionary with the data. """
    step1 = line.split('"')
    step2 = step1[0].split(' ')
    data = {"TS": step2[1], "PROT": step2[0], "SRCIP": step2[3], "COMMAND": step1[1], "AGENT": step1[3]}
    return data


if __name__ == "__main__":
    """ Main Loop: Reads the downloaded ELB Log Files, parses the lines, and writes to CSV file. """

    # Where to find the *.log.gz files...
    home = str(Path.home())
    root_dir = home + '/Downloads/elb-logs/'

    # Create CSV file
    with open('elb-access-log.csv', mode='w') as csv_file:
        fieldnames = ['TS', 'PROT', 'SRCIP', 'COMMAND', 'AGENT']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        # Iterate through all files recursively and process each line
        files_list = glob.iglob(root_dir + '**/*.log.gz', recursive=True)
        for filename in files_list:
            with gzip.open(filename, 'rt') as file:
                for line in file:
                    # Parse log line and write fields to csv file
                    writer.writerow(parse_line(line))
